/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yada.oxoop;

import java.util.Scanner;
/**
 *
 * @author ASUS
 */
public class OXOOP {
    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        Game game = new Game();
        game.showWelcome();
        game.newBoard();
        
        while (true) {
            game.showTable();
            game.showTurn();
            game.inputRowCol();
            if (game.isFinish()) {
                game.showTable();
                game.showResult();
                game.newBoard();
                game.showStat();
                if (CheckPlayAgain(kb, game)) break;
            }
        }
    }
    
    private static boolean CheckPlayAgain(Scanner kb, Game game) {
        System.out.print("Do you want to play again? (y/n): ");
        char x = kb.next().charAt(0);
        if (x == 'y') {
            System.out.println();
            game.newBoard();
        } else {
            return true;
        }
        return false;
    }



}
