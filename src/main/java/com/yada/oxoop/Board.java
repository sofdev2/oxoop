/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yada.oxoop;

/**
 *
 * @author ASUS
 */
public class Board {
    private char table[][] = {{'-','-','-'},{'-','-','-'},{'-','-','-'}};
    private Player currentPlayer;
    private Player o;
    private Player x;
    private int count;
    private int row,col;
    public boolean win = false;
    public boolean draw = false;

    
    public Board(Player o,Player x) {
        this.currentPlayer = o;
        this.o = o;
        this.x = x;
        this.count = 0;
    }
    
     public char[][] getTable() {
        return table;
    }

    public Player getCurrentPlayer() {
        return currentPlayer;
    }

    public Player getO() {
        return o;
    }

    public Player getX() {
        return x;
    }

    public int getCount() {
        return count;
    }

    public boolean setRowCol(int row, int col) {
        this.row = row;
        this.col = col;
        if(isWin() || isDraw()) return false;
        if (row > 3 || col > 3 || row < 1 || col < 1) {
            return false;
        }
        if (table[row - 1][col - 1] != '-') {
            return false;
        }
        table[row - 1][col - 1] = currentPlayer.getSymbol();
        if (checkWin(row, col)) {
            updateStatWin();
            this.win = true;
            return true;
        }
        if (updateStatDraw()) {
            return true;
        }

        count++;
        switchPlayer();

        return true;
    }
    
    private boolean updateStatDraw() {
        if (checkDraw()) {
            o.draw();
            x.draw();
            this.draw = true;
            return true;
        }
        return false;
    }

    
    private void updateStatWin() {
        if(this.currentPlayer==o){
            o.win();
            x.loss();
        }else{
            o.loss();
            x.win();
        }
    }

    
    public boolean isWin() {
        return win;
    }

    public boolean isDraw() {
        return draw;
    }

    
    public void switchPlayer() {
        if (currentPlayer == o) {
            currentPlayer = x;
        } else {
            currentPlayer = o;
        }
    }
    
    public boolean checkWin(int row, int col) {
        if (checkVertical()) {
            return true;
        } else if (checkHorizontal()) {
            return true;
        } else if (checkDiagnal()) {
            return true;
        }
        return false;
    }

    public boolean checkVertical() {
        for (int r = 0; r < table.length; r++) {
            if (table[r][col - 1] != currentPlayer.getSymbol()) {
                return false;
            }
        }
        return true;
    }

    private boolean checkHorizontal() {
        for (int c = 0; c < table.length; c++) {
            if (table[row - 1][c] != currentPlayer.getSymbol()) {
                return false;
            }
        }
        return true;
    }

    private boolean checkDiagnal() { 
        if (checkLeftDiagnal()) {
            return true;
        } else if (checkRightDiagnal()) { 
            return true;
        }
        return false;
    }

    private  boolean checkLeftDiagnal() {  // 11, 22, 33
        for (int i = 0; i < table.length; i++) {
            if (table[i][i] != currentPlayer.getSymbol()) {
                return false;
            }
        }
        return true;
    }

    private boolean checkRightDiagnal() {  // 13, 22, 31 => 02, 11, 20
        for (int i = 0; i < table.length; i++) {
            if (table[i][2 - i] != currentPlayer.getSymbol()) {
                return false;
            }
        }
        return true;
    }

     private boolean checkDraw() {
        if (count == 8) {
            return true;
        }
        return false;
    }



}
